FROM node:11.15.0-stretch-slim

#RUN apt-get update && apt-get install -y \
#    git \
# && rm -rf /var/lib/apt/lists/*

WORKDIR /app

ADD package.json ./
RUN npm install

ADD . ./
RUN npm run build:prod

ENTRYPOINT ["npm", "start"]

