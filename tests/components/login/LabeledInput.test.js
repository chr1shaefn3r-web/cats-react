import { mount } from 'enzyme'

import LabeledInput from '../../../components/login/LabeledInput'

describe('LabeledInput', () => {
    it('should render a native input element', () => {
        const wrapper = mount(<LabeledInput />)
        expect(wrapper.exists('input')).toBeTruthy
    })

    const props = {
        onChange: jest.fn()
    }
    it('should call onChange if input changes', () => {
        const mockInput = 'Hello, World!'
        const wrapper = mount(<LabeledInput {...props} />)

        const input = wrapper.find('input').first();
        input.simulate('change', { target: { value: mockInput } })

        expect(props.onChange).toHaveBeenCalledTimes(1);
        expect(props.onChange.mock.calls[0][0].target.value).toBe(mockInput)
    })
})
