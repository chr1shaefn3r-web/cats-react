import { mount } from 'enzyme'

import LoginForm from '../../../components/login/LoginForm'

describe('LoginForm', () => {
    it('should render a fieldset', () => {
        const wrapper = mount(<LoginForm />)
        expect(wrapper.exists('fieldset')).toBeTruthy
    })

    it('should call onSubmit if submit button is pressed', () => {
        const props = getProps();
        const wrapper = mount(<LoginForm {...props} />)

        const submitButton = wrapper.find('input[type="submit"]').first();
		submitButton.simulate('click');

        expect(props.onSubmit).toHaveBeenCalledTimes(1);
    })

    it('should call onChange when input fields are changed', () => {
        const props = {
            onChange: jest.fn()
        }
        const mockInput = "Mock Data"
        const wrapper = mount(<LoginForm {...props} />)

        const input = wrapper.find('input[type="text"]').first()
        input.simulate('change', { target: { value: mockInput } })

        expect(props.onChange).toHaveBeenCalledTimes(1);
        expect(props.onChange.mock.calls[0][0].target.value).toBe(mockInput)
    })

    function getProps() {
        return {
            onSubmit: jest.fn()
        }
    }
})
