import { mount } from 'enzyme'

import Footer from '../../../components/footer/Footer'

describe('Footer', () => {
    it('should render at least "Christoph"', () => {
        const wrapper = mount(<Footer />);
        expect(wrapper.text()).toContain("Christoph");
    })
})
