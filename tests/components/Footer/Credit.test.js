import { shallow } from 'enzyme'

import Credit from '../../../components/footer/Credit'

describe('Credit', () => {
    it('should render at least "Christoph"', () => {
        const wrapper = shallow(<Credit />);
        expect(wrapper.text()).toContain("Christoph");
    })
})
