# cats-react

> A digital version of our "which cat is in the house" installation at home

## Docker-based build

```bash
docker build -t cats-react-build . && bash scripts/docker-based.sh
```
