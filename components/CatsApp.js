import withinContainer from "./withinContainer"

import Loading from "../components/cats/LoadingCat"
import CatTiles from '../components/cats/CatTiles'
import Login from '../components/login/Login'
import Version from "../components/version/Version"
import Footer from "../components/footer/Footer"


const weAreRunningClientSide = () => typeof window !== "undefined";

class CatsApp extends React.Component {

    componentDidMount() {
        if (weAreRunningClientSide()) {
            console.log("Registered automatic reload")
            setInterval(() => {
                window.document.location.reload(true);
            }, 2 * 60 * 60 * 1000); // every 2h
        }
        if ("serviceWorker" in navigator) {
            navigator.serviceWorker.register("./sw.js")
            .then(() => console.log("Service worker registered"))
            .catch(err => console.error("Service worker registration failed", err))
        } else {
            console.log("Service Worker not supported")
        }
    }

    render() {
        const { cats, login, isCatsLoaded, isAuthEmpty, toggleCat, version } = this.props;
        return (
            <div>
                <h1>Katzenstatus</h1>

                {!isCatsLoaded && <Loading />}
                {isCatsLoaded && <CatTiles cats={cats} toggleCat={toggleCat} />}
                {isAuthEmpty && <Login login={login} />}

                <Version {...version}/>
                <Footer />
            </div>
        )
    }
}

export default withinContainer(CatsApp);
