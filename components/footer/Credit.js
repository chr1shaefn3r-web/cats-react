export default () => (
	<div>
		<p className="credit">&copy; Christoph Haefner</p>
		<style jsx>{`
			.credit {
				color: white;
				margin: 20px 0;
			}
		`}
		</style>
	</div>
)
