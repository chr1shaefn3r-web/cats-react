import Credit from "./Credit"

export default () => (
    <div>
        <div id="footer">
            <Credit />
        </div>
        <style jsx>{`
			#footer {
				background-color: #e09739;
				height: 60px
			}
		`}
        </style>
    </div>
)
