import DeceasedBadge from "./badge/DeceasedBadge"

export default class Deceased extends React.Component {

    render() {
        const catsImage = (size = "") => `./static/cats/${this.props.name.toLowerCase()}${size}-deceased.jpg`;
        return (
            <div className="inside">
                <button>
                    <img src={catsImage()} alt={this.props.name}
                        srcset={`${catsImage()} 100w, ${catsImage(150)} 150w, ${catsImage(200)} 200w`}
                        sizes="(max-width: 650px) 100px, (max-width: 850px) 150px, 200px" />
                </button>
                <DeceasedBadge text={this.props.date} />
                <style jsx>{`
                    .inside {
                        width: 100px;
                        margin-left: auto;
                        margin-right: auto;
                    }
                    button {
                        width: 100%;
                        height: 100px;
                        background-color: transparent;
                        border: none;
                        margin: 0px;
                        margin-bottom: 4px;
                        padding: 0px;
                    }
                    @media screen and (min-width: 650px) {
                        .inside {
                            width: 150px;
                        }
                        button {
                            height: 150px;
                        }
                    }
                    @media screen and (min-width: 850px) {
                        .inside {
                            width: 200px;
                        }
                        button {
                            height: 200px;
                        }
                    }
                `}
                </style>
            </div>
        )
    }
}
