import Cat from "./Cat"
import DeceasedCat from "./DeceasedCat"

const byOrder = ([, valueA], [, valueB]) => {
    return valueA.order - valueB.order;
}

const deceased = (name) => {
    return ([catName]) => catName !== name
}

export default ({ cats, toggleCat }) => (
    <div className="catTiles">
        <DeceasedCat key={"luna"} name={"luna"} date={"07.02.2024"} />
        <DeceasedCat key={"dusty"} name={"dusty"} date={"24.11.2024"} />
        {Object.entries(cats)
            .filter(deceased("luna"))
            .filter(deceased("dusty"))
            .sort(byOrder)
            .map(([key, value]) => (
                <Cat key={key} name={key} toggleCat={toggleCat} {...value} />
            ))}
        <style jsx>{`
			.catTiles {
                margin: 15px;
                display: flex;
                flex-direction: row;
                flex-wrap: wrap;
			}
		`}
        </style>
    </div>
)
