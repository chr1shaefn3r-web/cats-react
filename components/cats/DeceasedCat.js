import Deceased from "./Deceased"

export default (props) => (
	<div className="cat">
		<Deceased {...props} />
		<style jsx>{`
			.cat {
				flex-grow: 1;
				margin-bottom: 15px;
			}
		`}
		</style>
	</div>
)
