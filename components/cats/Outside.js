import { noFlip, animationDuration, nearlySeconds} from "./animationConstants"
import SuccessBadge from "./badge/SuccessBadge"

export default class Outside extends React.Component {

    state = {
        flip: noFlip,
        animationDuration
    }

    onButtonClick = (e) => {
        this.flipIt();
        setTimeout(() => {
            this.resetFlip();
            this.props.toggleCat(this.props.name.toLowerCase(), false)
        }, this.state.animationDuration * nearlySeconds);
    }

    resetFlip = () => {
        this.setState({ flip: noFlip });
    }

    flipIt = () => {
        this.setState({ flip: "flip" })
    }

    render() {
        return (
            <div className="outside">
                <button className={this.state.flip} onClick={this.onButtonClick}>{this.props.name}</button>
                <SuccessBadge text="Draußen" />
                <style jsx>{`
                    .outside {
                        width: 100px;
                        margin-left: auto;
                        margin-right: auto;
                    }
                    button {
                        width: 100%;
                        height: 100px;
                        background-color: rgb(240, 240, 240);
                        border: none;
                        margin: 0px;
                        margin-bottom: 4px;
                        padding: 0px;
                        text-transform:capitalize;
                    }
                    @media screen and (min-width: 650px) {
                        .outside {
                            width: 150px;
                        }
                        button {
                            font-size: x-large;
                            height: 150px;
                        }
                    }
                    @media screen and (min-width: 850px) {
                        .outside {
                            width: 200px;
                        }
                        button {
                            font-size: xx-large;
                            height: 200px;
                        }
                    }
                    .flip {
                        animation-duration: ${this.state.animationDuration}s;
                        animation-name: flip;
                    }
                    @keyframes flip {
                        from {
                            transform: none;
                        }
                        to {
                            transform: rotateY(90deg) translate3d(0,0,4px);
                        }
                    }
                `}
                </style>
            </div>
        )
    }
}
