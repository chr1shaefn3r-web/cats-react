export default () => (
    <div className="root">
        <img id="loadingCat" src="./static/cat-walking.gif" alt="Loading Cat" />
        <style jsx>{`
            .root {
                text-align: center;
                margin: 15px;
            }
            #loadingCat {
				max-width: 32%;
                border-radius: 1rem;
                box-shadow: 8px 8px 10px #161616;
            }
		`}
        </style>
    </div>
)