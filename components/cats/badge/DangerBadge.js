import Badge from "./Badge"

export default ({ text }) => <Badge text={text} backgroundColor="#dc3545" />
