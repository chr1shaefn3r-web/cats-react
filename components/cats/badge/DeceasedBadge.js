import Badge from "./Badge"
// Latin cross
export default ({ text }) => <Badge text={`✝ ${text}`} backgroundColor="#000" />
