export default ({ text, backgroundColor }) => (
    <div className="badge">
        {text}
        <style jsx>{`
            .badge {
                color: #fff;
                background-color: ${backgroundColor};
                width: 100%;
                display: inline-block;
                padding: .25em 0em;
                font-family: Sans-Serif;
                font-size: 75%;
                font-weight: 700;
                line-height: 1;
                text-align: center;
                white-space: nowrap;
                vertical-align: baseline;
                border: none;
            }
        `}
        </style>
    </div>
)