import Badge from "./Badge"

export default ({ text }) => <Badge text={text} backgroundColor="#28a745" />
