import Inside from "./Inside"
import Outside from "./Outside"

export default ({ name, inside, toggleCat }) => (
    <div className="cat">
        {inside && <Inside name={name} toggleCat={toggleCat} />}
        {!inside && <Outside name={name} toggleCat={toggleCat} />}
		<style jsx>{`
			.cat {
				flex-grow: 1;
				margin-bottom: 15px;
			}
		`}
		</style>
    </div>
)
