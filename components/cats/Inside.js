import { noFlip, animationDuration, nearlySeconds } from "./animationConstants"
import DangerBadge from "./badge/DangerBadge"

export default class Inside extends React.Component {

    state = {
        flip: noFlip,
        animationDuration
    }

    onButtonClick = (e) => {
        this.flipIt();
        setTimeout(() => {
            this.resetFlip();
            this.props.toggleCat(this.props.name.toLowerCase(), true)
        }, this.state.animationDuration * nearlySeconds);
    }

    resetFlip = () => {
        this.setState({ flip: noFlip });
    }

    flipIt = () => {
        this.setState({ flip: "flip" })
    }

    render() {
        const catsImage = (size = "") => `./static/cats/${this.props.name.toLowerCase()}${size}.jpg`;
        return (
            <div className="inside">
                <button className={this.state.flip} onClick={this.onButtonClick}>
                    <img src={catsImage()} alt={this.props.name}
                        srcset={`${catsImage()} 100w, ${catsImage(150)} 150w, ${catsImage(200)} 200w`}
                        sizes="(max-width: 650px) 100px, (max-width: 850px) 150px, 200px" />
                </button>
                <DangerBadge text="Drinnen" />
                <style jsx>{`
                    .inside {
                        width: 100px;
                        margin-left: auto;
                        margin-right: auto;
                    }
                    button {
                        width: 100%;
                        height: 100px;
                        background-color: transparent;
                        border: none;
                        margin: 0px;
                        margin-bottom: 4px;
                        padding: 0px;
                    }
                    @media screen and (min-width: 650px) {
                        .inside {
                            width: 150px;
                        }
                        button {
                            height: 150px;
                        }
                    }
                    @media screen and (min-width: 850px) {
                        .inside {
                            width: 200px;
                        }
                        button {
                            height: 200px;
                        }
                    }
                    .flip {
                        animation-duration: ${this.state.animationDuration}s;
                        animation-name: flip;
                    }
                    @keyframes flip {
                        from {
                            transform: none;
                        }
                        to {
                            transform: rotateY(90deg) translate3d(0,0,4px);
                        }
                    }
                `}
                </style>
            </div>
        )
    }
}
