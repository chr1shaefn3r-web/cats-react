import LabeledInput from "./LabeledInput"

export default ({ onSubmit, onChange }) => (
	<div className="root">
		<fieldset>
			<legend>Login with eMail and password</legend>

			<LabeledInput text="eMail" name="email" type="text" onChange={onChange} />

			<LabeledInput text="Password" name="password" type="password" onChange={onChange} />

			<input type="submit" value="Sign in" onClick={onSubmit}/>

		</fieldset>
		<style jsx>{`
			.root {
				color: #757575;
            }
            fieldset {
                border-color: #BDBDBD;
                border-style: dashed;
            }
			legend {
					padding: 3px 6px;
			}
			input {
				display: inline-block;
				text-align: center;
				vertical-align: middle;
				border: 1px solid transparent;
				padding: .25rem;
				font-size: 1rem;
				line-height: 1.5;
				border-radius: .5rem;
			}
			input:hover {
				color: #fff;
				background-color: #5a6268;
				border-color: #545b62;
			}
		`}
		</style>
	</div>
)
