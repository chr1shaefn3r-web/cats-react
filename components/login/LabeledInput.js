export default ({name, text, type, onChange}) => (
    <div className="root">
        <input type={type} id={name} name={name} onChange={onChange} placeholder={text}/>

		<style jsx>{`
			.root {
				margin-bottom: 1rem !important;
				position: relative;
				display: flex;
				flex-wrap: wrap;
				align-items: stretch;
				width: 100%;
			}

			input {
					position: relative;
					flex: 1 1 auto;
					margin-bottom: 0;
					padding: .4rem;
					background-color: #fff;
					background-clip: padding-box;
					border: 1px solid #ced4da;
					border-radius: .5rem;
			}
		`}
		</style>
    </div>
)