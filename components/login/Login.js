import React from "react";

import LoginForm from "./LoginForm";

export default class Login extends React.Component {

    state = {
        email: "",
        password: "",
        loginError: ""
    }

    constructor(props) {
        super(props);
        this.login = props.login.bind(this);
    }

    setValue = (e) => {
        this.setState({ [e.target.name]: e.target.value });
    }

    onSubmit = () => {
        this.login(this.state.email, this.state.password)
            .then(() => {
                this.setState({ loginError: "" });
            })
            .catch((err) => {
                this.setState({ loginError: err });
            });
    }

    render() {
        return (
            <div>
                <LoginForm onSubmit={this.onSubmit} onChange={this.setValue} />
                {Boolean(this.state.loginError) && JSON.stringify(this.state.loginError, null, 2)}
            </div>
        );
    }
}