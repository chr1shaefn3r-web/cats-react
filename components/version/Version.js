import halfWidth from "./responsiveHalfWidth"
import GitLink from "./GitLink"

const Version = (version) => (
    <div className="root">
        <fieldset>
			<legend>Version Info</legend>
            Build {version.time.substr(0,10)} by {version.deployer.name} on {version.deployer.machine} from <GitLink {...version.repo} /> &nbsp;via
            '{version.pipeline}' pipeline as v{version.versionString}
        </fieldset>
        <style jsx>{`
			.root {
				color: #757575;
            }
            fieldset {
                border-color: #BDBDBD;
                border-style: dashed;
            }
			legend {
					padding: 3px 6px;
			}
		`}
        </style>
    </div>
)

export default halfWidth(Version);
