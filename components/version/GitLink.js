import generateWebUrl from "./generateWebUrl"
export default ({ gitHead, url }) => {
    const webUrl = generateWebUrl(url, gitHead);
    return (
        <span className="root">
            <a href={webUrl}>{gitHead.substr(0, 6)}</a>
            <style jsx>{`
                .root > a {
                    color: #428bca;
                    text-decoration: none;
                }
                .root > a:hover, .root > a:focus {
                    color: #2a6496;
                    text-decoration: underline;
                }
                .root > a:hover, .root > a:active {
                    outline: 0;
                }
                .root > a:focus {
                    outline: #333 dotted thin;
                    outline: -webkit-focus-ring-color auto 5px;
                    outline-offset: -2px;
                }
            `}
            </style>
        </span>
    )
}