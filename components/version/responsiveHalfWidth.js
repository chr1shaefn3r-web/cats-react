export default (WrappedComponent) => props => (
    <div className="halfWidth">
        <WrappedComponent {...props} />
        <style jsx>{`
			.halfWidth {
				width:50%
            }
            @media (max-width: 600px) {
                .halfWidth {
                    width: 75%
                }
            }
            @media (max-width: 400px) {
                .halfWidth {
                    width: 100%
                }
            }
		`}
        </style>
    </div>
)
