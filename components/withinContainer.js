export default (WrappedComponent) => props => (
	<div className="container">
		<WrappedComponent {...props} />
		<style jsx>{`
			.container {
				margin-right: auto;
				margin-left: auto;
				padding-left: 15px;
				padding-right: 15px;
				max-width: 950px;
			}
		`}
		</style>
	</div>
)
