#!/bin/bash

set -e # Exit immediately if a simple command exits with a non-zero status

id=$(docker create cats-react-build)
docker cp $id:/app/ - > app.tar
docker rm -v $id
rm -rf out && mkdir -p out && tar xf app.tar app/out --directory out/ --strip-components=1
rm app.tar
