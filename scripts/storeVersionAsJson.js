#!/usr/bin/env node
"use strict";

const PIPELINE_ARGUMENT_INDEX = 0;

const v = {};
v.versionString = process.env.npm_package_version;
v.name = process.env.npm_package_name;
v.deployer = {
	name: process.env.USER,
	machine: require("os").hostname()
};
v.repo = {
	url: process.env.npm_package_repository_url,
	gitHead: process.env.npm_package_gitHead
};
v.time = new Date().toLocaleString('de-DE');
v.pipeline = JSON.parse(process.env.npm_config_argv).remain[PIPELINE_ARGUMENT_INDEX];

process.stdout.write(JSON.stringify(v)+"\n");
