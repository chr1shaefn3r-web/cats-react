module.exports = {
  "globDirectory": "out/",
  "globPatterns": [
    "static/**",
    "_next/**",
    "index.html",
  ],
  "swDest": "out/sw.js",
  "swSrc": "static/sw.js"
};

