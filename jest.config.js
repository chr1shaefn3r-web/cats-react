module.exports = {
	  setupFiles: ['<rootDir>/jest.setup.js'],
	  testPathIgnorePatterns: ['<rootDir>/.next/', '<rootDir>/node_modules/'],
	  "testMatch": ["<rootDir>/tests/**/*.test.js"],
	  "collectCoverageFrom": [
		  "components/**/*.js",
	  ],
	  "coverageThreshold": {
		  "global": {
			  "statements": 11,
			  "branches": 20,
			  "functions": 12,
			  "lines": 11
		  }
	  },
	  "coverageDirectory": "coverage",
	  "bail": true
}

