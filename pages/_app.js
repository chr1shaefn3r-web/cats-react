import App from 'next/app'
import React from 'react'
import Head from 'next/head'

class CatsApp extends App {
  render() {
    const { Component, pageProps, reduxStore } = this.props
    return (
      <>
        <Head>
          <title>Katzenstatus</title>
          <meta name="viewport" content="initial-scale=1.0, width=device-width" />

          <meta name="theme-color" content="#e09739" />
          <link href="static/cats-96.png" rel="icon" type="image/png" />
          <link rel="manifest" href="static/manifest.webmanifest" />
        </Head>
        <Component {...pageProps} />
        <style jsx global>{`
          body {
            background: #e09739
          }
          h1 {
            color: white;
          }
        `}
        </style>
      </>
    )
  }
}

export default CatsApp
