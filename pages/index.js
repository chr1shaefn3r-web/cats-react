import { useFirebase, useFirebaseConnect, isLoaded, isEmpty } from 'react-redux-firebase';
import { useSelector } from 'react-redux'
import { withRedux } from '../lib/redux';

import CatsApp from "../components/CatsApp"

const IndexPage = () => {
	useFirebaseConnect([
		{ path: 'cats' }
	])

	const cats = useSelector(state => state.firebase.data.cats)
	const auth = useSelector(state => state.firebase.auth)
	const version = useSelector(state => state.version)

	const firebase = useFirebase()

	const toggleCat = (catKey, currentInsideState) => {
		firebase.update(`/cats/${catKey}/`, {
			inside: !currentInsideState
		})
	}

	const login = (email, password) => firebase.login({ email, password });

	const catsAppProps = {
		cats: cats,
		isCatsLoaded: isLoaded(cats),
		isAuthEmpty: isEmpty(auth),
		toggleCat,
		login,
		version
	};
	return (
		<CatsApp {...catsAppProps} />
	)
}

export default withRedux(IndexPage);
