import { createStore, combineReducers } from 'redux'
import firebase from 'firebase'
import { firebaseReducer } from 'react-redux-firebase'
import { firebaseConfig } from '../configs'
import versionReducer from './versionReducer'

export const reducer = combineReducers({
  firebase: firebaseReducer,
  version: versionReducer
})

if (!firebase.apps.length) {
  firebase.initializeApp(firebaseConfig);
}

export function initializeStore(initialState = {}) {
  return createStore(
    reducer,
    initialState
  )
}
