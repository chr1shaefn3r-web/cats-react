importScripts('https://storage.googleapis.com/workbox-cdn/releases/4.1.1/workbox-sw.js');

if (workbox) {
    workbox.precaching.precacheAndRoute([],
        new workbox.strategies.StaleWhileRevalidate()
    );
}
